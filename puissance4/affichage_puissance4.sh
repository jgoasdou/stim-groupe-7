#!/bin/bash

afficher_grille()
{
	echo -e "\t\t 1   2   3   4   5   6   7"
	echo -e "\t\t╔═══╦═══╦═══╦═══╦═══╦═══╦═══╗"
	for j in {1..7}
	do
		i=$((8-$j))
		l1="${c1[$i]}"
		l2="${c2[$i]}"
		l3="${c3[$i]}"
		l4="${c4[$i]}"
		l5="${c5[$i]}"
		l6="${c6[$i]}"
		l7="${c7[$i]}"
		echo -ne "\t\t║"
		for k in {1..7}
		do
			l="l$k"
			var=${!l}
			#On va choisir la couleur avec laquelle écrire:
			if [ -z $var ]
			then
				echo -n "   "
			elif [ $var == "X" ]
			then
				echo -ne '\033[1;31m ● \033[0m'
			elif [ $var == "O" ]
			then
				echo -ne '\033[1;33m ● \033[0m'
			fi
			
			if [ $k -lt 7 ]
			then
				echo -n "║"
			fi
		done
		echo "║"
		if [ $j != 7 ]
		then
			echo -e "\t\t╠═══╬═══╬═══╬═══╬═══╬═══╬═══╣"
		else
			echo -e "\t\t╚═══╩═══╩═══╩═══╩═══╩═══╩═══╝"
			echo -e "\t\t  1   2   3   4   5   6   7"
		fi
	done
}

afficher_grille_bw()
#Pour l'affichage dans whiptail
{
	echo -e "\t\t 1   2   3   4   5   6   7"
	echo -e "\t\t╔═══╦═══╦═══╦═══╦═══╦═══╦═══╗"
	for j in {1..7}
	do
		i=$((8-$j))
		l1="${c1[$i]}"
		l2="${c2[$i]}"
		l3="${c3[$i]}"
		l4="${c4[$i]}"
		l5="${c5[$i]}"
		l6="${c6[$i]}"
		l7="${c7[$i]}"
		echo -ne "\t\t║"
		for k in {1..7}
		do
			l="l$k"
			var=${!l}
			#On va choisir la couleur avec laquelle écrire:
			if [ -z $var ]
			then
				echo -n "   "
			elif [ $var == "X" ]
			then
				echo -n ' X '
			elif [ $var == "O" ]
			then
				echo -n ' O '
			fi
			
			if [ $k -lt 7 ]
			then
				echo -n "║"
			fi
		done
		echo "║"
		if [ $j != 7 ]
		then
			echo -e "\t\t╠═══╬═══╬═══╬═══╬═══╬═══╬═══╣"
		else
			echo -e "\t\t╚═══╩═══╩═══╩═══╩═══╩═══╩═══╝"
			echo -e "\t\t  1   2   3   4   5   6   7"
		fi
	done
}

afficher_infos_joueurs()
{
	if [ $(($joueur % 2)) = 1 ]
	then
		color="\033[1;31m ● \033[0m"
	else
		color="\033[1;33m ● \033[0m"
	fi
	echo -e "\t\t\v\v\v╔══════════╗\t\t╔═══╗"
	echo -e "\t\t║ Joueur $((joueur % 2 +1)) ║\t\t║`echo "$color"`║"
	echo -e "\t\t╚══════════╝\t\t╚═══╝"
	echo ""

}
