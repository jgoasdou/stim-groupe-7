#!/bin/bash

declare -a c1=("0")
declare -a c2=("0")
declare -a c3=("0")
declare -a c4=("0")
declare -a c5=("0")
declare -a c6=("0")
declare -a c7=("0")

#Jeu initialisé avec la première en 0 pour faciliter l'accès par la suite

init()
{
	joueur=1;

	c1=("0")
	c2=("0")
	c3=("0")
	c4=("0")
	c5=("0")
	c6=("0")
	c7=("0")
}


check()
{
#Vérifie que le joueur peut bein jouer dans la colonne qu'il souhaite
	temp='${#'"c$1"'[*]}'
	eval taille=$temp
        if [ $1 -lt 1 ] || [ $1 -gt 8 ]
	then
		return 2
	elif [ $taille -lt 8 ]
        then
                echo "$1"
                return 0 
        else
                echo "0"
                return 1
        fi
}

jouer()
{	
	clear
	if [ $(($joueur % 2)) = 1 ]
	then
		pion="X"
	else
		pion="O"
	fi 
	#Afficher la grille
	#Call de la fonction d'affichage
	afficher_infos_joueurs
	afficher_grille


	#Choix de la colonne où jouer
	colonne=0
	while [ $colonne = 0 ] || [ $colonne -gt 7 ] || [ $colonne -lt 1 ]
	do
		read -p $'\nDans quel colonne voulez-vous jouer? Colonne: ' colonne
		case $colonne in
			[1-7])
				colonne=`check $colonne`
				if [ $? = 1  ]
				then
					clear
					afficher_infos_joueurs
					afficher_grille
					echo -e "\t\t\v\033[1;37;41m Colonne invalide \033[0m"
				fi
				;;
			*)
				colonne=0
				clear
				afficher_infos_joueurs
				afficher_grille
				echo -e "\t\t\v\033[1;37;41m Colonne invalide \033[0m"
				;;
		esac
	done

	#Si le joueur arrive ici, le coup est possible
	#Le joueur joue le coup:

	var="c$colonne"
	eval $var+="($pion)"
	#Le tableau de la colonne (eg c1) est mis à jour

	#Commande de vérification: vérifie la victoire ou le blocage de la partie
	#Faire après le passage à l'autre joueur

	#C'est à l'autre de jouer !
	joueur=$(($joueur+1))

	points=`check_victory $colonne`
	full=`check_full`

	if [ $points -ge 4 ]
	then
		whiptail --title "Stim launcher - Gagné !!!" --msgbox "`afficher_grille_bw`" 24 80
		init
		#Remise à zéro du jeu
		menu
	elif [  $full = 1 ]
	then
		whiptail --title "Égalité" --msgbox "Vous avez rempli la grille, plus aucun coup n'est possible\nIl y a égalité\n'`afficher_grille_bw`" 24 80
		init
		menu
	else
		#echo $joueur
		#Si partie pas finie, appel de jouer
		jouer
	fi
}

