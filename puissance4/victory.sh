#!/bin/bash

check_victory()
{
	dernier_coup=$1
	#Checker la ligne horizontale
	colonne=$dernier_coup
	max=`check_line $dernier_coup 1 0`
	points=`check_line $dernier_coup 0 1`
	if [ $points -gt $max ]
	then
		max=$points
	fi
	points=`check_line $dernier_coup 1 1`
	if [ $points -gt $max ]
	then
		max=$points
	fi
	points=`check_line $dernier_coup 1 -1`
	if [ $points -gt $max ]
	then
		max=$points
	fi
	echo "$max"
}

check_full()
{
	full=true
	for i in {1..7}
	do
		temp='${#'"c$i"'[*]}'
		eval ligne=$temp
		if [ $ligne -lt 8 ]
		then
			full=false
		fi
	done
	if [ $full = true ]
	then
		echo "1"
		return 1
	else
		echo "0"
		return 0
	fi
}

check_line()
{
	#Trpos paramètres: colonne actuelle différence pour la ligne et pour la colonne
	colonne=$1
	temp='${#'"c$colonne"'[*]}'
	eval ligne=$temp
	#On exclue le 0
	ligne=$(($ligne-1))
	temp='${'"c$colonne""[$ligne]}"
	eval pion=$temp
	diff_colonne=$3
	diff_ligne=$2

	#A gauche
	count=1
	left=true
	#echo "Check à gauche"
	#echo "Colonne: $colonne | Ligne : $ligne"
	while [ $left = true ]
	do

		if [ $(($colonne-$diff_colonne)) -lt 1 ] || [ $(($colonne-$diff_colonne)) -gt 7 ] || [ $(($ligne-$diff_ligne)) -lt 1 ] || [ $(($ligne-$diff_ligne)) -gt 7 ]
		then
			#echo "Trop petit ou trop grand"
			left=false
		else
			colonne=$(($colonne-$diff_colonne))
			ligne=$(($ligne-$diff_ligne))
			var='${'"c$colonne""[$ligne]}"
			eval nouveau_pion=$var
			#Pour éviter une erreur
			if [ -z $nouveau_pion ]
			then
				nouveau_pion="0"
			fi
			#echo "Pion: $pion, $nouveau_pion"
			if [ $nouveau_pion = $pion ]
			then
				count=$(($count+1))
			else
				left=false
			fi
		fi
	done
	#A droite
	colonne=$1
	temp='${#'"c$colonne"'[*]}'
	eval ligne=$temp
	#On exclue le 0
	ligne=$(($ligne-1))
	right=true
	#echo "Check à droite"
	#echo "Colonne: $colonne | Ligne : $ligne"
	while [ $right = true ]
	do
		if [ $(($colonne+$diff_colonne)) -lt 1 ] || [ $(($ligne+$diff_ligne)) -lt 1 ] || [ $(($ligne+$diff_ligne)) -gt 7 ] || [ $(($colonne+$diff_colonne)) -gt 7 ]
		then
			#echo "Trop petit ou trop grand"
			right=false
		else
			colonne=$(($colonne+$diff_colonne))
			ligne=$(($ligne+$diff_ligne))
			var='${'"c$colonne""[$ligne]}"
			eval nouveau_pion=$var
			if [ -z $nouveau_pion ]
			then
				nouveau_pion="0"
			fi
			#echo "Pion: $pion, $nouveau_pion"
			if [ $nouveau_pion = $pion ]
			then
				count=$(($count+1))
			else
				right=false
			fi
		fi
	done

	#echo "Bravo: $count points"
	echo "$count"
}
