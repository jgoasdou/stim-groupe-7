#!/usr/bin/env bash
declare -a tab

## $1 le nombre de tours
## $2 le numéro du joueur
## $3 le tableau

affichage_morpion(){
  clear
  tab=($3)

  for (( i = 0; i < 9; i++ )); do
    var="var$i"
    if [ ${tab[$i]} = '0' ]; then
      eval var$i="\ "
    elif [ ${tab[$i]} = '1' ]; then
      eval var$i="X"
    elif [ ${tab[$i]} = '2' ]; then
      eval var$i="O"
    else
      eval var$i=${tab[$i]}
    fi
  done


    echo  "
    ┌───────────┐           ┌─────────────┐
    │ Tour : $1  │           │ Joueur : $2  │
    └───────────┘           └─────────────┘


             A   B   C
           ╔═══╦═══╦═══╗
         1 ║ $var0 ║ $var1 ║ $var2 ║
           ╠═══╬═══╬═══╣
         2 ║ $var3 ║ $var4 ║ $var5 ║
           ╠═══╬═══╬═══╣
         3 ║ $var6 ║ $var7 ║ $var8 ║
           ╚═══╩═══╩═══╝  "
  }

refresh(){
    clear
    affichage_morpion $1 $2
}

## tour_de_jeu_morpion( nom_joueur1 nom_joueur2 1||2)
## $3 = 1 si c'est au joueur 1 de jouer et 2 si c'est au joueur 2 de jouer

tour_de_jeu_morpion(){
  if [ -z $1 ] || [ -z $2 ] || [ -z $3 ] ; then
    echo "Il manque des arguments dans l'appel de la fonction tour_de_jeu_morpion"
    return -1
  else
    player1=$1
    player2=$2
    tour=$3
    if [ $3 = 2 ] ; then
      echo " "
      echo "C'est à $player2 de jouer ..."
    elif [ $3 = 1 ]; then
      echo " "
      echo "C'est à $player1 de jouer ..."
    else
      echo "  '$3' est invalide"
      return -1
    fi
    echo " "
    echo "Renseignez la case où vous souhaitez jouer : "
  fi
}

afficher_win(){
  clear

    echo "
                ~═════════════════════════════~
                      Le gagnant est : $1
                ~═════════════════════════════~
    "


}
