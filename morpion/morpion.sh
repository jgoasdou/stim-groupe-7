#!/bin/bash

tab=("0" "0" "0" "0" "0" "0" "0" "0" "0")
source ./affichage_morpion.sh

game_is_finished_victory(){
    win=0
    for i in  `seq 0 8`
    do
        if [ ${tab[$i]} = $tour ]
        then
            #Case n°1
            if [ $i = 0 ]
            then
                if [ ${tab[$(( $i + 1 ))]} = $tour ] &&  [ ${tab[$(( $i + 2 ))]} = $tour ]
                then
                    win=1
                elif [ ${tab[$(( $i + 3 ))]} = $tour ] &&  [ ${tab[$(( $i + 6 ))]} = $tour ]
                then
                    win=1
                elif [ ${tab[$(( $i + 4 ))]} = $tour ] &&  [ ${tab[$(( $i + 8 ))]} = $tour ]
                then
                    win=1
                fi
            #Case n°2
            elif [ $i = 1 ]
            then
                if [ ${tab[$(( $i - 1 ))]} = $tour ] &&  [ ${tab[$(( $i + 1 ))]} = $tour ]
                then
                    win=1
                elif [ ${tab[$(( $i + 3 ))]} = $tour ] &&  [ ${tab[$(( $i + 6 ))]} = $tour ]
                then
                    win=1
                fi
            #Case n°3
            elif [ $i = 2 ]
            then
                if [ ${tab[$(( $i - 1 ))]} = $tour ] &&  [ ${tab[$(( $i - 2 ))]} = $tour ]
                then
                    win=1
                elif [ ${tab[$(( $i + 3 ))]} = $tour ] &&  [ ${tab[$(( $i + 6 ))]} = $tour ]
                then
                    win=1
                elif [ ${tab[$(( $i + 2 ))]} = $tour ] &&  [ ${tab[$(( $i + 4 ))]} = $tour ]
                then
                    win=1
                fi
            #Case n°4
            elif [ $i = 3 ]
            then
                if [ ${tab[$(( $i + 1 ))]} = $tour ] &&  [ ${tab[$(( $i + 2 ))]} = $tour ]
                then
                    win=1
                elif [ ${tab[$(( $i - 3 ))]} = $tour ] &&  [ ${tab[$(( $i + 3 ))]} = $tour ]
                then
                    win=1
                fi
            #Case n°5
            elif [ $i = 4 ]
            then
                if [ ${tab[$(( $i - 1 ))]} = $tour ] &&  [ ${tab[$(( $i + 1 ))]} = $tour ]
                then
                    win=1
                elif [ ${tab[$(( $i - 3 ))]} = $tour ] &&  [ ${tab[$(( $i + 3 ))]} = $tour ]
                then
                    win=1
                elif [ ${tab[$(( $i - 4 ))]} = $tour ] &&  [ ${tab[$(( $i + 4 ))]} = $tour ]
                then
                    win=1
                elif [ ${tab[$(( $i - 2 ))]} = $tour ] &&  [ ${tab[$(( $i + 2 ))]} = $tour ]
                then
                    win=1
                fi
            #Case n°6
            elif [ $i = 5 ]
            then
                if [ ${tab[$(( $i - 1 ))]} = $tour ] &&  [ ${tab[$(( $i - 2 ))]} = $tour ]
                then
                    win=1
                elif [ ${tab[$(( $i - 3 ))]} = $tour ] &&  [ ${tab[$(( $i + 3 ))]} = $tour ]
                then
                    win=1
                fi
            #Case n°7
            elif [ $i = 6 ]
            then
                if [ ${tab[$(( $i + 1 ))]} = $tour ] &&  [ ${tab[$(( $i + 2 ))]} = $tour ]
                then
                    win=1
                elif [ ${tab[$(( $i - 3 ))]} = $tour ] &&  [ ${tab[$(( $i - 6 ))]} = $tour ]
                then
                    win=1
                elif [ ${tab[$(( $i - 2 ))]} = $tour ] &&  [ ${tab[$(( $i - 4 ))]} = $tour ]
                then
                    win=1
                fi
            #Case n°8
            elif [ $i = 7 ]
            then
                if [ ${tab[$(( $i - 1 ))]} = $tour ] &&  [ ${tab[$(( $i + 1 ))]} = $tour ]
                then
                    win=1
                elif [ ${tab[$(( $i - 3 ))]} = $tour ] &&  [ ${tab[$(( $i - 6 ))]} = $tour ]
                then
                    win=1
                fi
            #Case n°9
            elif [ $i = 8 ]
            then
                if [ ${tab[$(( $i - 1 ))]} = $tour ] &&  [ ${tab[$(( $i - 2 ))]} = $tour ]
                then
                    win=1
                elif [ ${tab[$(( $i - 3 ))]} = $tour ] &&  [ ${tab[$(( $i - 6 ))]} = $tour ]
                then
                    win=1
                elif [ ${tab[$(( $i - 4 ))]} = $tour ] &&  [ ${tab[$(( $i - 8 ))]} = $tour ]
                then
                    win=1
                fi
            fi
        fi

    done
return $win
}


start_multi_game(){
    tab=("0" "0" "0" "0" "0" "0" "0" "0" "0")
    #Start multi Game
    player1=$1
    player2=$2
    game_running=1
    tour=1
    nb_coups=0
    while [ $game_running -eq 1 ]
    do

        if [ $tour -eq 1 ];
        then

            affichage_morpion $nb_coups $tour "${tab[*]}"
            tour_de_jeu_morpion $player1 $player2 $tour
            read coup
            case $coup in
                      A1) val=0;;
                      B1) val=1;;
                      C1) val=2;;
                      A2) val=3;;
                      B2) val=4;;
                      C2) val=5;;
                      A3) val=6;;
                      B3) val=7;;
                      C3) val=8;;
                  esac
            if [ ${tab[$val]} -eq 0 ]
            then
              tab[$val]=1
              game_is_finished_tie
              game_is_finished
              if [ $? = 1 ] ;  then
                afficher_win $player1
                sleep 3s
                menu
              fi
                    tour=2
                    nb_coups=$(( $nb_coups + 1 ))

            fi
        elif [ $tour -eq 2 ]
        then
            affichage_morpion $nb_coups $tour "${tab[*]}"
            tour_de_jeu_morpion $player1 $player2 $tour
            read coup
            case $coup in
                A1) val=0;;
                B1) val=1;;
                C1) val=2;;
                A2) val=3;;
                B2) val=4;;
                C2) val=5;;
                A3) val=6;;
                B3) val=7;;
                C3) val=8;;
            esac
            if [ ${tab[$val]} -eq 0 ]
            then
                tab[$val]=2
                game_is_finished_tie
                game_is_finished
                if [ $? = 1 ] ;  then
                  afficher_win $player2
                  sleep 3s
                  menu
                fi
               tour=1
               nb_coups=$(( $nb_coups + 1 ))

            fi
        fi

    done
}


game_is_finished(){
    game_is_finished_tie
    game_is_finished_victory
}

game_is_finished_tie(){
    if [ $nb_coups -ge 8 ]
    then
        game_running=0
        echo "DRAW!"
        sleep 2s
        menu
    fi
}




start_solo_game(){
    echo "solo"
}




if [ $1 = "solo" ]
then
    start_solo_game
elif [ $1 = "multi" ]
then
    start_multi_game $2 $3
else
    echo "Error starting game"
fi
