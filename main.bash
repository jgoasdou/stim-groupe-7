#!/bin/bash

source puissance4/jeu.sh

source puissance4/affichage_puissance4.sh
source puissance4/victory.sh

source morpion/morpion.sh
source morpion/affichage_morpion.sh

## RESUME : ######################################

display_pause()
{
    choix=$(whiptail --title "Pause Menu" --menu "Choose an option" 24 80 16 3>&1 1>&2 2>&3 \
    "Back" "Return to the main menu." \
    "Resume" "Get back into the game" \
    "Exit" "End the game and close terminal")
    
    if [ -z $choix ]
    then
        echo "GoodBye"
        exit 1
    fi
    
    case $choixMorpion in
        Back)
            menu
        ;;
        Resume)
            display_resuming
        ;;
        Exit)
            echo "GoodBye"
            exit 1 #ne fonctionne pas vrmt -> retour au menu
        ;;
    esac
}

display_resuming()
{
    {
    for ((i = 0 ; i <= 100 ; i+=5)); do
        sleep 0.1
        echo $i
    done
    } | whiptail --gauge "Resuming Game" 6 50 0 #bien vérif retour au jeu ensuite
}

## GAME : ######################################

endGame()
{
    pseudoJoueur1="basaxter"
    pseudoJoueur2="nsbluez"

    if [ ! -d jeux ]
    then
        mkdir jeux
        cd jeux
        touch $pseudoJoueur1.txt
        touch $pseudoJoueur2.txt
        echo "$pseudoJoueur1," >> $pseudoJoueur1.txt
        echo "$pseudoJoueur2," >> $pseudoJoueur2.txt
    else
        cd jeux
        if [ ! -e $USER.txt ]
        then
            touch $USER.txt
            echo "$USER," >> $USER.txt
        fi
    fi
}

morpionPvP()
{
    #ici
    start_multi_game Joueur1 Joueur2
}

morpionPvIA()
{
    echo "-> enter morpion in PvIA"
}

morpion()
{   
    whiptail --title "Stim launcher" --msgbox --scrolltext "Morpion is a turn-based game played on a checkerboard of 3 squares by 3 squares. The objective is very simple, just align 3 symbols to win. The game is played as follows: a first player draws a symbol on an empty square. The first player to line up 3 symbols wins the game, if there are no more free squares and no winner, it's a tie. \n                                A   B   C \n
                              ╔═══╦═══╦═══╗\n
                            1 ║   ║   ║   ║\n
                              ╠═══╬═══╬═══╣\n
                            2 ║   ║   ║   ║\n
                              ╠═══╬═══╬═══╣\n
                            3 ║   ║   ║   ║\n
                              ╚═══╩═══╩═══╝" 24 80

    choixMorpion=$(whiptail --title "Stim launcher" --menu "Morpion option selection" 24 80 16 3>&1 1>&2 2>&3 \
    "PvP" "Player vs Player" \
    "PvIA" "Player vs IA" \
    "Return" "↩️")

    if [ -z $choixMorpion ]
    then
        echo "GoodBye"
        exit 1
    fi

    case $choixMorpion in
       PvP)
            morpionPvP
        ;;
        PvIA)
            morpionPvIA
        ;;
        Return)
            play
        ;;
    esac
}

puissance4PvP()
{
    echo "-> enter puissance4 in PvP"
    init
    jouer

}

puissance4PvIA()
{
    echo "-> enter puissance4 in PvIA"
}

puissance4()
{   
    whiptail --title "Stim launcher" --msgbox "The rules are simple: Try to build a row of four checkers while keeping your opponent from doing the same. Sounds easy, but it's not! The vertical strategy creates a unique challenge: you must think in a whole new way to block your opponent's moves!\n
`afficher_grille_bw`" 30 80

    choixPuissance4=$(whiptail --title "Stim launcher" --menu "Puissance4 option selection" 24 80 16 3>&1 1>&2 2>&3 \
    "PvP" "Player vs Player" \
    "PvIA" "Player vs IA" \
    "Return" "↩️")

    if [ -z $choixPuissance4 ]
    then
        echo "GoodBye"
        exit 1
    fi

    case $choixPuissance4 in
       PvP)
            puissance4PvP
        ;;
        PvIA)
            puissance4PvIA
        ;;
        Return)
            play
        ;;
    esac
}

play()
{
    choixPlay=$(whiptail --title "Stim launcher" --menu "Game selection" 24 80 16 3>&1 1>&2 2>&3 \
    "Morpion" "v1" \
    "Puissance4" "v1" \
    "Return" "↩️")

    if [ -z $choixPlay ]
    then
        echo "GoodBye"
        exit 1
    fi

    case $choixPlay in
       Morpion)
            morpion
        ;;
        Puissance4)
            puissance4
        ;;
        Return)
            menu
        ;;
    esac
}

stats()
{   
    dataStats=""
    playerList=$(ls jeux/)
    for i in $playerList
    do
        dataBrut=$(cat jeux/$i)
        pseudo=$(echo $dataBrut | cut -d , -f1)
        mWin=$(echo $dataBrut | cut -d , -f2)
        mLoose=$(echo $dataBrut | cut -d , -f3)
        mTail=$(echo $dataBrut | cut -d , -f4)
        mWinrate=$((100*$mWin/($mLoose+$mTail+$mWin)))
        pWin=$(echo $dataBrut | cut -d , -f5)
        pLoose=$(echo $dataBrut | cut -d , -f6)
        pTail=$(echo $dataBrut | cut -d , -f7)
        pWinrate=$((100*$pWin/($pLoose+$pTail+$pWin)))
        dataStats="${dataStats} \n $pseudo : Morpion : ($mWin/$mLoose/$mTail - $mWinrate%) | Puissance4 : ($pWin/$pLoose/$pTail - $pWinrate%)"
    done
    whiptail --title "Stim launcher" --msgbox --scrolltext "Statistics (win/loose/tie - Winrate %)\n $dataStats" 24 80
    menu
}

menu()
{      
    choix=$(whiptail --title "Stim launcher" --menu "Menu" 24 80 16 3>&1 1>&2 2>&3 \
    "Play" "🎮" \
    "Statistics" "📊" \
    "Exit" "🚪")

    if [ -z $choix ]
    then
        echo "GoodBye"
        exit 1
    fi

    case $choix in
       Play)
            play 
        ;;
        Statistics)
            stats
        ;;
        Exit)
            echo "GoodBye"
            exit 1
        ;;
    esac
}

menu

#PAUSE : -> display_pause
