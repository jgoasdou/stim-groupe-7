#!/bin/bash

USER=$(whiptail --inputbox "What is your user name?" 24 80 --title "Login" 3>&1 1>&2 2>&3)

exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "User entered " $USER
else
    echo "User selected Cancel."
fi

cd

if [ ! -d jeux ]
then
	mkdir jeux
	cd jeux
	touch $USER.txt
	echo "$USER," >> $USER.txt
else
	cd jeux
	if [ ! -e $USER.txt ]
	then
		touch $USER.txt
		echo "$USER," >> $USER.txt
	fi
cd
fi
