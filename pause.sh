#!/bin/bash
display_pause()
{
    choix=$(whiptail --title "Pause Menu" --menu "Choose an option" 24 80 16 3>&1 1>&2 2>&3 \
    "Back" "Return to the main menu." \
    "Resume" "Get back into the game" \
    "Exit" "End the game and close terminal")
    
    if [ -z $choix ]
    then
        echo "choix vide"
        exit
    else
        echo "choix non vide"
    fi

    if [ $choix = "Back" ]
    then
        echo "Call menu function"
    elif [ $choix = "Resume" ]
    then
        display_resuming
    elif [ $choix = "Exit" ]
    then
        exit 1
    fi
    echo "(Exit status was $choix)"
}

display_resuming()
{
    {
    for ((i = 0 ; i <= 100 ; i+=5)); do
        sleep 0.1
        echo $i
    done
    } | whiptail --gauge "Resuming Game" 6 50 0
}

#read -p "What are you planning on doing ? " action

#if [ $action = "p" ]
#then
#    display_pause
#fi

GetKeystroke () {
oldSttySettings=`stty -g`
stty -echo raw
echo "`dd count=1 2> /dev/null`"
stty $oldSttySettings
}

action="X"
while [ `echo "DHPQS"  | grep -v $action` ]
do
  action=`GetKeystroke | cut -c1 | tr "[a-z]" "[A-Z]"`
done
if [ $action = "P" ]
then
    display_pause
fi







